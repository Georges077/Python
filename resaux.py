#!/usr/bin/env python
import socket
import sys
sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address=('localhost', 10000)
sock.bind(server_address)
sock.listen(1)
while True:
   print>>sys.stderr,'nous attendons connexions'
   connection, client_address=sock.accept()
   try: 
      print>>sys.stderr, 'connexion depuis ', client_address
      while True:
           data=connection.recv(16)
           print>>sys.stderr, 'nous aurons recu %s' %data
           if data:
	      print>>sys.stderr, 'Renvoi des donnees'
	      connection.sendall(data)
           else:
	      print>> sys.stderr, 'il n y a plus de donnees depuis ', client_address
	      break
   finally:
       connection.close()
